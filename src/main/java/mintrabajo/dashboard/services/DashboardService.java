package mintrabajo.dashboard.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mintrabajo.dashboard.Entity.DashboardDTO;
import mintrabajo.dashboard.Entity.DashboardEntity;
import mintrabajo.dashboard.helper.DataHelper;
import mintrabajo.dashboard.repository.DashboardRepository;
import mintrabajo.dashboard.singleton.DashboardDataSingleton;

@Service
public class DashboardService {
	private DashboardRepository pr;
	private DashboardDataSingleton instance = DashboardDataSingleton.getInstance();
    @Autowired
    public void setPr(DashboardRepository pr) {
         this.pr = pr;
    }
    
    public List<DashboardEntity> insumoDashboard() {
        List<Object[]> liObj = pr.insumoDashBoard();
        List<DashboardEntity> entity= new ArrayList<>();
        //por ahora solo los 10 primeros registros
        for (int i=0; i<10; i++) {
            entity.add(DataHelper.objToDashboardEntity(liObj.get(i)));
        }
        return entity;
    }
    public DashboardDTO generarDashboardDTO() {
    	
    	DashboardDTO dashboardDTO = instance.getDashBoard();
    	if(dashboardDTO != null) {
    		return dashboardDTO;
    	} else {
    		dashboardDTO = new DashboardDTO();
    	}
    	
    	dashboardDTO.setActivos(DataHelper.getLongFromCounter(pr.dashboardEstado(DataHelper.ESTADO_ACTIVO)));
    	dashboardDTO.setFinalizados(DataHelper.getLongFromCounter( pr.dashboardEstado(DataHelper.ESTADO_FINALIZADO)));
    	dashboardDTO.setTotal((dashboardDTO.getActivos()+dashboardDTO.getFinalizados()));
    	dashboardDTO.setDespacho(DataHelper.getLongFromCounter(pr.dashboarDespacho(DataHelper.GRUPO_RL)));
    	dashboardDTO.setIvc(DataHelper.getLongFromCounter(pr.dashboardOrganigrama(DataHelper.GRUPO_NL, "SI","NO")));
    	dashboardDTO.setRcc(DataHelper.getLongFromCounter(pr.dashboardOrganigrama(DataHelper.GRUPO_NL, "NO","SI")));
    	dashboardDTO.setIvcRcc(DataHelper.getLongFromCounter(pr.dashboardOrganigrama(DataHelper.GRUPO_NL, "SI","SI")));
    	dashboardDTO.setTotalDespacho(dashboardDTO.getDespacho()+dashboardDTO.getIvc()+dashboardDTO.getRcc()+dashboardDTO.getIvcRcc());
    	dashboardDTO.setExpedientesConCaducidad(DataHelper.getLongFromCounter(pr.dashboardExpedientesConCaducidad()));
    	dashboardDTO.setExpedientesVencenSemana(DataHelper.getLongFromCounter(pr.dashboardExpedientesConCaducidad(0, 7)));
    	dashboardDTO.setExpedientesVencenMes(DataHelper.getLongFromCounter(pr.dashboardExpedientesConCaducidad(8, 30)));
    	dashboardDTO.setExpedientesVencenTrimestre(DataHelper.getLongFromCounter(pr.dashboardExpedientesConCaducidad(31, 90)));
    	instance.setDashBoard(dashboardDTO);
    	return dashboardDTO;
    }
}
