package mintrabajo.dashboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mintrabajo.dashboard.Entity.DashboardDTO;
import mintrabajo.dashboard.Entity.DashboardEntity;
import mintrabajo.dashboard.services.DashboardService;

@RestController
@RequestMapping(path = "dashboard/")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.OPTIONS})

public class DashboardController {
	private DashboardService ps;

    @Autowired
    public void setPs(DashboardService ps) {
        this.ps = ps;
    }
    
    @GetMapping(path = "",produces="application/json")
    @ResponseBody
    public List<DashboardEntity>   dashboard() {
        return ps.insumoDashboard();
    }
    @GetMapping(path = "init",produces="application/json")
    @ResponseBody
    public DashboardDTO   dashboardDTO() {
        return ps.generarDashboardDTO();
    }

}
