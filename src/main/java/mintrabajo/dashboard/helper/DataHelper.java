package mintrabajo.dashboard.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mintrabajo.dashboard.Entity.DashboardEntity;

public class DataHelper {
	
	public static final String ESTADO_ACTIVO = "ACTIVO";
	public static final String ESTADO_FINALIZADO = "FINALIZADO";
	public static final String GRUPO_RL = "RIESGOS LABORALES";
	public static final String GRUPO_NL = "NORMAS LABORALES";
	private static final Long RENUENCIA = 15691L;
	private static final Long PAS = 3245L;
	private static final Long AP = 3218L;

	public static DashboardEntity objToDashboardEntity(Object[] obj) {
		DashboardEntity data= new DashboardEntity();
		int i = 0;
		data.setIdExpediente(longObj(obj[i]));
		i++;
		data.setNumeroRadicacion(stringObj(obj[i]));
		i++;
		data.setOrigenActuacion(stringObj(obj[i]));
		i++;
		data.setEstadoProceso(stringObj(obj[i]));
		i++;
		data.setNaturalezaInvestigacion(stringObj(obj[i]));
		i++;
		data.setResumenQuerellantes(stringObj(obj[i]));
		i++;
		data.setResumenQuerellados(stringObj(obj[i]));
		i++;
		data.setEstadoActividad(stringObj(obj[i]));
		i++;
		data.setPerfilActivo(stringObj(obj[i]));
		i++;
		data.setActividadActual(stringObj(obj[i]));
		i++;
		data.setEtapaActual(stringObj(obj[i]));
		i++;
		data.setEtapaActualDesc(stringObj(obj[i]));
		i++;
		data.setFlujoActual(stringObj(obj[i]));
		i++;
		data.setCadenaFlujo(stringObj(obj[i]));
		i++;
		data.setOrganigramaRadicado(stringObj(obj[i]));
		i++;
		data.setGrupoRepartoInicial(stringObj(obj[i]));
		i++;
		data.setTerritorialRepartoInicial(stringObj(obj[i]));
		i++;
		data.setUsuarioReparto(stringObj(obj[i]));
		i++;
		data.setOrganigramaNombreReparto(stringObj(obj[i]));
		i++;
		data.setUsuarioDirectorioActivo(stringObj(obj[i]));
		i++;
		data.setUsuarioEstado(stringObj(obj[i]));
		i++;
		data.setCiuuQuerellado(stringObj(obj[i]));
		i++;
		data.setCiuuCodigoQuerellado(stringObj(obj[i]));
		i++;
		data.setSectorCriticoQuerellado(stringObj(obj[i]));
		i++;
		data.setSectorCriticoDescQuerellado(stringObj(obj[i]));
		i++;
		data.setTipoRecurso(stringObj(obj[i]));
		i++;
		data.setFechaRadicacion(dateObj(obj[i]));
		i++;
		data.setFechaHechos(dateObj(obj[i]));
		i++;
		data.setFechaHechosFin(dateObj(obj[i]));
		i++;
		data.setFechaPrescripcion(dateObj(obj[i]));
		i++;
		data.setFechaPreasignacionReparto(dateObj(obj[i]));
		i++;
		data.setFechaReparto(dateObj(obj[i]));
		i++;
		data.setValorSancion(longObj(obj[i]));
		i++;
		data.setTipoDecision(stringObj(obj[i]));
		i++;
		data.setCriterioGraduacionSancion(stringObj(obj[i]));
		i++;
		data.setEjecutante(stringObj(obj[i]));
		i++;
		data.setInfraccion(stringObj(obj[i]));
		i++;
		data.setTipoInfraccion(stringObj(obj[i]));
		i++;
		data.setTipoSancion(stringObj(obj[i]));
		i++;
		data.setMateriaConducta(stringObj(obj[i]));
		i++;
		data.setFlujoPk(longObj(obj[i]));
		return data;
	}
	
	private static String stringObj(Object obj) {
		if(obj == null) {
			return "";
		}else {
			return obj.toString();
		}
	}
	
	private static Long longObj(Object obj) {
		if(obj == null) {
			return 0L;
		} else {
			try {
				return Long.parseLong(obj.toString());
			} catch(Exception e) {
				return 0L;
			}
			
		}
	}
	private static Date dateObj(Object obj) {
		if(obj == null) {
			return null;
		} else {
			try {
			return (Date) obj;
			}catch (Exception e) {
				return null;
			}
		}
	}
	
	public static Long getLongFromCounter(List<Object[]>  count) {
		try {
			Object[] obj = count.get(0);
			BigDecimal objs = (BigDecimal) obj[0];
			return objs.longValue();
		}catch (Exception e) {
			return 0L;
		}
		
	}
}
