package mintrabajo.dashboard.singleton;

import java.util.Date;

import mintrabajo.dashboard.Entity.DashboardDTO;

public class DashboardDataSingleton {
	private static DashboardDataSingleton instance;
	private static Long dashboardCreationTime;
	private static final Long validTime = 7200000L;
	private static DashboardDTO dashboardDTO;
	private DashboardDataSingleton() {
		
	}
	
	public static DashboardDataSingleton getInstance() {
		if (instance == null ) {
			instance = new DashboardDataSingleton();
					
		}
		return instance;
	}
	
	public  void setDashBoard(DashboardDTO dto) {
		Date date = new Date(); 
		dashboardCreationTime = date.getTime();	
		dashboardDTO = dto;
	}
	public  DashboardDTO getDashBoard() {
		Date date = new Date(); 
		Long current = date.getTime();	
		if (dashboardDTO == null || current > (dashboardCreationTime+validTime) ) {
			return null;
		} else {
			return dashboardDTO;
		}
	}
}
